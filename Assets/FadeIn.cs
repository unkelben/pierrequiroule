﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FadeIn : MonoBehaviour {

    public bool fading = false;
    // Use this for initialization
	void Start () {
        StartCoroutine("fadein");
	}
	

   public IEnumerator fadein()
    {
        fading = true;
        while (GetComponent<Image>().color.a > 0.05f)
        {
            // Debug.Log("alpha: " + Name.color);
            GetComponent<Image>().color = GetComponent<Image>().color - new Color(0, 0, 0, 0.01f);
            yield return new WaitForSeconds(0.01f);
        }
        fading = false;
    }
   public IEnumerator fadeout(string NextScene)
    {
        fading = true;
        while (GetComponent<Image>().color.a < 0.95f)
        {
            // Debug.Log("alpha: " + Name.color);
            GetComponent<Image>().color = GetComponent<Image>().color + new Color(0, 0, 0, 0.01f);
            yield return new WaitForSeconds(0.01f);
        }
        fading = false;
        if (NextScene != "")
        {
            SceneManager.LoadScene(NextScene);
        }
    }

}

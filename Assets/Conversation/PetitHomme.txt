1	Jacques			Ha  ha!	PetitHomme++	Ha ha!	
2	Maître		!PetitHomme	De quoi riais-tu tout à l’heure?	PetitHomme++	Rire?	
2	Maître		PetitHomme	Tu ris?	PetitHomme++	Rire?	
3	Jacques		PetitHomme	"Toutes les fois que je me rappelle le petit homme criant, jurant, écumant, se débattant de la tête, des pieds, des mains, de tout le corps, je ne saurais m’empêcher d’en rire."	PetitHomme++	Petit Homme	
4	Maître		!PetitHomme	Qui était le petit homme criant et jurant ? Quelqu’un que je connais?	PetitHomme	Petit Homme?	
4	Maître		PetitHomme	"Et ce petit homme, qui est-il ? Quelqu’un que je connais?"	PetitHomme	Petit Homme?	
4	Jacques		PetitHomme	Non.	PetitHomme++	Non	
5	Maître		!PetitHomme	Dis-moi seulement qui était le petit homme.	PetitHomme	Petit Homme?	
5	Maître		PetitHomme	Qui est-il donc?	PetitHomme	Petit Homme?	
5	Jacques		PetitHomme	"Un jour un enfant, assis au pied du comptoir d’une lingère, criait de toute sa force."	PetitHomme++;KeepTalking	Un jour…	garcon
6	Jacques		PetitHomme	"La marchande importunée de ses cris, lui dit : « Mon ami, pourquoi criez-vous ? »"	PetitHomme++;KeepTalking	Pourquoi?	
7	Jacques		PetitHomme	« C’est qu’ils veulent me faire dire A. »	PetitHomme++	A	
8	Maître		!PetitHomme	Pourquoi l’enfant ne voulait-il pas dire A ?	PetitHomme	A?	
8	Maître		PetitHomme	Et pourquoi ne voulait-il pas dire A ?	PetitHomme	A?	
8	Jacques		PetitHomme	"« C’est que je n’aurai pas si tôt dit A, qu’ils voudront me faire dire B… »"	PetitHomme++;KeepTalking	B	
9	Jacques		PetitHomme	"C’est que je ne vous aurai pas si tôt dit le nom du petit homme, qu’il faudra que je vous dise le reste."	PetitHomme++	Reste	
10	Maître		PetitHomme	Peut-être	PetitHomme	Peut-être	
10	Jacques		PetitHomme	Cela est sûr.	PetitHomme++	Sûr	

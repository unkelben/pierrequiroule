﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public Transform guy;
    public float speed = 0.5f;
    Vector3 RelativePosition = new Vector3();
	// Use this for initialization
	void Start () {
        RelativePosition = transform.position - guy.position;
		
	}
	
	// Update is called once per frame
	void Update () {
        if (guy.position.y > -100) { 
            Vector3 CurrentRelativePosition = transform.position - guy.position;
            transform.position += (RelativePosition - CurrentRelativePosition) * speed;
        }
    }
}

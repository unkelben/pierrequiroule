﻿
public class DialogueLine  {

    public string character;
    public string topiclevel;
    public string condition;
    public string output;
    public string text;
    public string topic;
    public string activetopic;
    public string shorttext;
    public int warmth;
    public string illustration;
    

    public DialogueLine(string atopic, string atopiclevel, string acharacter, string acondition,  string aactivetopic, string atext, string aoutput, string ashorttext, int awarmth, string aillustration)
    {
        character = acharacter;
        topiclevel = atopiclevel;
        condition = acondition;
        output = aoutput;
        text = atext;
        topic = atopic;
        activetopic = aactivetopic;
        shorttext = ashorttext;
        warmth = awarmth;
        illustration = aillustration;
    }
}

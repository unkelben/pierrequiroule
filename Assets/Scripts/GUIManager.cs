﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GUIManager : MonoBehaviour {

    public string JacquesName;
    public string MaitreName;
    public Transform ConversationPanel;
    public Transform ButtonPanel;
    public RectTransform ButtonPrefab;
    public RectTransform NextPrefab;
    public Transform DialogueLinePrefab;
    public Transform IllustrationPrefab;
    ConversationManager CM;
    public List<Transform> Repliques;
    public List<Sprite> TopicChangeIllustrations;
    public List<Sprite> Illustrations;
    string LastCharacter = "";

    //State machine stuff
    int GUIState = 0;
    //0 = startup
    //1 = Repliques are moving up
    //2 = Wait/ing for repliques
    //3 = Waiting for player


    public int RepliquesMoving = 0;
    float MoveStep = 0;
    DialogueLine LastLine;
    bool WaitingForImage = false;

    // Use this for initialization
	void Start () {

        Repliques = new List<Transform>();
        CM = GameObject.Find("ConversationManager").GetComponent<ConversationManager>();

        //NPC launches conversation
        NPCTalk();
        //Debug.Log("nom du sprite: " + Illustrations[0].name);
    }
	
	// Update is called once per frame
	void Update () {

        //check if new replique is all set up
        if (GUIState == 2)
        {
            if(Repliques[Repliques.Count-1].GetComponent<RectTransform>().rect.height > 0)
            {
                //if it's an image, resize to image size
                if (Repliques[Repliques.Count - 1].GetComponent<UnityEngine.UI.Image>() != null)
                {
                    UnityEngine.UI.Image im = Repliques[Repliques.Count - 1].GetComponent<UnityEngine.UI.Image>();
                    //find the ratio of the original
                    float imheight = im.sprite.texture.height;
                    float imwidth = im.sprite.texture.width;
                    float imratio = imheight/imwidth;
                    RectTransform rt = Repliques[Repliques.Count - 1].GetComponent<RectTransform>();
                   // Debug.Log("imratio :" + imratio + "sizedeltax :"+rt.sizeDelta.x);
                    //resize to fit current width and ratio
                    rt.sizeDelta = new Vector2(rt.sizeDelta.x,rt.sizeDelta.x*imratio);
                }
                MoveRepliques();
                GUIState = 1;
            }
        }
        //repliques stopped moving?
       if ((GUIState == 1) && (RepliquesMoving < 1))
        {
            RepliquesMoving = 0;
            GUIState = 0;
            //fade in the new one
            Repliques[Repliques.Count - 1].GetComponent<RepliqueBehavior>().StartFade();
            PrepareButtons();
                      
        }

		
	}

   public void SayLine(DialogueLine dialogueLine)
    {
        //Are we moving from intro?
        if (CM.GetTopicValue("intro") > 2)
        {
            StartGame();
            return;
        }



        //if this is the first line, add the first image
        if (GUIState == 0)
        {
            //dialogueLine.illustration = "jacques";
            //if (PlayerPrefs.GetInt("state") == 1) { }
        }
        
        //delete buttons
        foreach (RectTransform button in ButtonPanel)
        {
            if (button.name.Contains("Button"))
            {
                Destroy(button.gameObject);
            }

        }
        //are we returning from displaying an image?
        if (WaitingForImage) {
            dialogueLine = LastLine;
            WaitingForImage = false;
        }
        else
        {
            //save this dialogueline
            LastLine = dialogueLine;
        }

        bool sayline = true;
        //if last line contained illustration, display it
        if (dialogueLine.illustration != "")
        {
           // Debug.Log("line contains image: "+ dialogueLine.illustration);
            //find corresponding illustration
            int ImageIndex = 99999;
            foreach(Sprite illustration in Illustrations)
            {
                
                if (dialogueLine.illustration.Contains(illustration.name)){
                   // Debug.Log("showing " + illustration.name);
                    ImageIndex = Illustrations.IndexOf(illustration);
                }
            }
            if (ImageIndex != 99999)
            {
                Transform tempImage = Instantiate(IllustrationPrefab, ConversationPanel);
                tempImage.GetComponent<UnityEngine.UI.Image>().sprite = Illustrations[ImageIndex];
                tempImage.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -2000);
                Repliques.Add(tempImage);
                LastLine.illustration = "";
                LastLine.warmth = 1;
                WaitingForImage = true;
                //we don't want to see anything twice (?)
                //Illustrations.Remove(Illustrations[ImageIndex]);
                sayline = false;
            }

        }
        //if no illustration and subject change, add image
       /* else if ((dialogueLine.warmth < 1)&&(dialogueLine.character=="Jacques")&&(Random.value>0.0)&&(TopicChangeIllustrations.Count>0))
        {
            Debug.Log("adding topic change image");
            Transform tempImage = Instantiate(IllustrationPrefab, ConversationPanel);
            int ImageIndex = UnityEngine.Random.Range(0, TopicChangeIllustrations.Count);
            tempImage.GetComponent<UnityEngine.UI.Image>().sprite = TopicChangeIllustrations[ImageIndex];
            tempImage.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -2000);
            Repliques.Add(tempImage);
            LastLine.warmth = 1;
            WaitingForImage = true;
            //we don't want to see anything twice (?)
            TopicChangeIllustrations.Remove(TopicChangeIllustrations[ImageIndex]);
            sayline = false;
        }*/
        if(sayline)
        {
            //update the conversation manager
           // Debug.Log("regular line");
            CM.SayLine(dialogueLine);

            //create new visual "réplique"
            Transform tempreplique = Instantiate(DialogueLinePrefab, ConversationPanel);
            tempreplique.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -2000);

            string character;
            if (dialogueLine.character == "Jacques")
            {
                character = JacquesName;
            }
            else
            {
                character = MaitreName;
                Scene scene = SceneManager.GetActiveScene();
                if ((scene.name != "splash") && (PlayerPrefs.GetFloat("state") != 1))
                {
                    CM.SaveState();
                }
            }
           /* if (character == LastCharacter)
            {
                character = "";
            }
            else
            {
                LastCharacter = character;
            }*/
            
            

            tempreplique.GetComponent<RepliqueBehavior>().Texte.text = "<b>" + character + "</b>\n" + dialogueLine.text;
            Repliques.Add(tempreplique);

            //Si c'est le maître, changer de couleur
            if (dialogueLine.character == "Maître")
            {
                tempreplique.GetComponent<UnityEngine.UI.Text>().color = new Color(0.5686275f, 0.1098039f, 0.1098039f);
            }

        }
        //trigger moving up
        GUIState = 2;

        if (CM.ActiveTopic.Contains("fin"))
        {
            Debug.Log("End game!");
            EndGame();
            return;
        }

    }

 

    void PrepareButtons()
    {
        List<RectTransform> boutons = new List<RectTransform>();

        //delete all buttons
        int numbuttons = 0;

        if (CM.CurrentSpeaker == "Maître")
        {
            CM.GetAvailableLines();
            if (CM.CurrentAvailableLines[0].text == "Fin")
            {
                CM.CurrentAvailableLines[0].activetopic = "fin";
                CM.CurrentAvailableLines[0].output = "fin++";
                CM.CurrentAvailableLines[0].text = "Fin";
            }
            foreach (DialogueLine line in CM.CurrentAvailableLines)
            {
                RectTransform tempButton = Instantiate(ButtonPrefab, ButtonPanel);
                tempButton.GetComponent<DialogueButton>().SetButton(line);
                boutons.Add(tempButton);
                numbuttons++;

            }
        }
        else
        {
            RectTransform tempButton = Instantiate(NextPrefab, ButtonPanel);
            boutons.Add(tempButton);
            numbuttons++;
        }
        float buttonwidth = 360;
        buttonwidth = (ButtonPanel.GetComponent<RectTransform>().rect.width / boutons.Count) * 0.9f;
        if (buttonwidth > 500)
            buttonwidth = 500;
        // Debug.Log("screen width : "+ ButtonPanel.GetComponent<RectTransform>().rect.width + " button width : " + buttonwidth);
        foreach(RectTransform bout in boutons)
        {
            bout.sizeDelta = new Vector2(buttonwidth, bout.sizeDelta.y);
        }
        GUIState = 3;
    }

    void MoveRepliques()
    {

        //if an image was added, push latest replique down.
        float MoveStep = Repliques[Repliques.Count - 1].GetComponent<RectTransform>().sizeDelta.y + 100;
   

        
           
        foreach(Transform replique in Repliques)
        {
            replique.GetComponent<RepliqueBehavior>().movestep = MoveStep;
            if (replique!= Repliques[Repliques.Count - 1])
            {
                replique.GetComponent<RepliqueBehavior>().MoveUp();
                RepliquesMoving++;
            }


        }
        
    }

    public void NPCTalk()
    {
       // Debug.Log("jacque's turn");

        
        //get started by finding lines for npc and displaying one
        CM.GetAvailableLines();
        //are there any available lines?

        if (CM.CurrentAvailableLines.Count > 0)
        {
            //Choose a random line to say
            int RandomLine = -1;
            while (RandomLine == -1)
            {
                RandomLine = Mathf.RoundToInt(UnityEngine.Random.Range(0, CM.CurrentAvailableLines.Count));
                //if the line is "cold", small chance to keep it
                if (CM.CurrentAvailableLines[RandomLine].warmth < 1)
                {
                    if (Random.value < 0.50)
                    {
                        RandomLine = -1;
                    }
                }
            }
            
            
            //Debug.Log("Random Line: " + RandomLine);

            SayLine(CM.CurrentAvailableLines[RandomLine]);
        }
    }
    public void Intro()
    {
        PlayerPrefs.SetFloat("state", 0);
        StartCoroutine(GameObject.Find("Fader").GetComponent<FadeIn>().fadeout("intro"));
        //yield return new WaitForSeconds(2);
       
        
    }
    public void StartGame()
    {
        PlayerPrefs.SetFloat("state", 0);
        PlayerPrefs.DeleteAll();
        StartCoroutine(GameObject.Find("Fader").GetComponent<FadeIn>().fadeout("Conversation"));
    }
    public void ContinueGame()
    {
        PlayerPrefs.SetFloat("state", 1);
        StartCoroutine(GameObject.Find("Fader").GetComponent<FadeIn>().fadeout("Conversation"));
    }
    public void EndGame()
    {
        PlayerPrefs.SetFloat("state", 0);
        StartCoroutine(GameObject.Find("Fader").GetComponent<FadeIn>().fadeout("fin"));
    }
}

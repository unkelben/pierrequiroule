﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDevWare.Dynamic.Expressions.CSharp;
using System;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ConversationManager : MonoBehaviour {

    //source files
    public TextAsset[] Textfiles;

    //variables
    public List<Topic> Topics = new List<Topic>();

    //useful
    public string ActiveTopic = "";
    bool topicswitch = false;
    public string CurrentSpeaker = "Jacques";
    public List<DialogueLine> AllLines = new List<DialogueLine>();
    public List<DialogueLine> CurrentAvailableLines = new List<DialogueLine>();
    public string[] RepriseImmediate;
    DialogueLine LastLine;
    

    // Use this for initialization
    void Start () {

//        PlayerPrefs.DeleteAll();
        //manually add all variables
        Topics.Add(new Topic("KeepTalking", 0, 0));
        Topics.Add(new Topic("fin", 0, 0));

        //Parse files to add dialogue lines
        foreach (TextAsset file in Textfiles)
        {
            //each file is a topic
            
            Topics.Add(new Topic(file.name,0,-10));
            string temptext = file.ToString();
           // Debug.Log(temptext);
            string[] lines = temptext.Split(Environment.NewLine.ToCharArray());
            foreach(string line in lines)
            {
                if (line != "")
                {
                    string[] chunks = line.Split(new Char[] { '\t' });
                    //purge text of ""
                    char[] charsToTrim = { '"', '"' };
                    chunks[4] = chunks[4].Trim(charsToTrim);
                    string illustration = "";
                    if (chunks.Length>7)
                    {
                        illustration = chunks[7];
                    }
                    AllLines.Add(new DialogueLine(file.name,chunks[0],chunks[1],chunks[2], chunks[3], chunks[4], chunks[5], chunks[6],-10, illustration));
                }
                
            }
        }
        
        bool haskey = PlayerPrefs.HasKey("state");
        if (haskey)
        {
            
            if (PlayerPrefs.GetFloat("state") == 1)
            {
                Debug.Log("state = 1 ");
                Scene scene = SceneManager.GetActiveScene();
                if (!(scene.name == "splash"))
                {
                    RestoreState();
                }
                
                PlayerPrefs.SetFloat("state", 0);
            }
        }
        else
        {
            PlayerPrefs.SetFloat("state", 0);
        }


    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown("space"))
        {
            ScreenCapture.CaptureScreenshot("d:/temp/screen");
        }
	}

    public void GetAvailableLines()
    {
        CurrentAvailableLines.Clear();
        foreach (DialogueLine line in AllLines)
        {
            bool notgood = false;

            //find out if good speaker
            if(!(line.character == CurrentSpeaker))
            {
                notgood = true; 
            }

            //find out if it's the good topic level
            //Debug.Log("line's topic: " + line.topic + " level: " + line.topiclevel + " | Current topic level: " + GetTopicValue(line.topic));
            int output=0;
            bool result = int.TryParse(line.topiclevel, out output);
            if (GetTopicValue(line.topic) != output) 
            {
                //Debug.Log("line rejected for wrong topic level");
                notgood = true;
            }

            //If there's a condition, find out if condition is true;
            //first parse all variables mentioned in conditions and replace them with current values
            string condition = line.condition;
            if (condition != "")
            {
                foreach (Topic topic in Topics)
                {
                    // let's find which variables this expression is about
                    if (condition.Contains(topic.name))
                    {
                        //Let's replace the variable in the expression by its current value
                        condition = condition.Replace(topic.name, topic.value.ToString());
                        //Debug.Log("condition : " + condition);

                    }
                }
                //evaluate parsed condition
                if (!CSharpExpression.Evaluate<bool>(condition))
                {
                    notgood = true;
                }
            }
            
            //find out if we meet the active topic criteria
            string[] demandedtopics = line.activetopic.Split(new Char[] { ';' });
            //for OR conditions, keep track of how many "notgoods" we get
            int notgoods = 0;
            
            foreach(String dtopic in demandedtopics)
            {
               /* if (CurrentSpeaker == "Jacques")
                {
                    Debug.Log("demanded topic : " + dtopic + "and active topics are: "+ActiveTopic);
                }*/
                
                //find out which topic we're talking about
                foreach (Topic topic in Topics)
                {
                    if (dtopic.Contains(topic.name))
                    {
                        //Debug.Log("found topic: " + topic.name + "of active: "+topic.active);
                        line.warmth = topic.active;
                        //this is the one
                        //do we want it active or not?
                        if (dtopic.Contains("!"))
                        {
                            if (topic.active>0)
                            {
                               // notgood = true;
                                notgoods++;
                               // Debug.Log("Line rejected because wrong topic active condition");
                            }

                        }
                        else
                        {
                            if (topic.active<1)
                            {

                                notgoods++;
                                //Debug.Log("Line rejected because wrong topic active condition");
                            }
                        }
                    }
                 }
                
            }

            /*if ((LastLine!=null)&&(LastLine.warmth < 1))
            {
                notgoods++;
            }*/
            if (notgoods == demandedtopics.Length)
            {
                notgood = true;
            }




            //add to availablelines
            if (!notgood)
            {
                //last test, prevent jacques topic switching on a topic switch
                if(!((LastLine!=null)&&(LastLine.warmth<1)&&(line.warmth<1)&&(line.character=="Jacques")))
                {
                    //so another last test: no topic switch when "keeptalking!"
                    if (!((GetTopicActive("KeepTalking") > 0)&&(line.warmth<1)))
                    {
                        CurrentAvailableLines.Add(line);
                    }
                        
                }




               // Debug.Log("added line: " + line.text);
            }

        }

        //DId we even find any good lines?
        if (CurrentAvailableLines.Count < 1)
        {
            if (CurrentSpeaker == "Jacques")
            {
                CurrentAvailableLines.Add(new DialogueLine("", "0", "Jacques", "", "", "...", "", "...",-10,""));
            }
            else
            {
                CurrentAvailableLines.Add(new DialogueLine("", "0", "Maître", "", "", "Fin", "", "Fin",-10,""));
            }
            
        }

        //Are there more than 3 options?
        //If so: rank by warmth, take the 2 warmest, random for last one
        if (CurrentAvailableLines.Count > 3)
        {
            CurrentAvailableLines = CurrentAvailableLines.OrderByDescending(dialogueline => dialogueline.warmth).ToList();
        }
        while (CurrentAvailableLines.Count > 3)
        {
            CurrentAvailableLines.RemoveAt(CurrentAvailableLines.Count - 1);
        }



    }

   public  void SayLine(DialogueLine dialogueLine)
    {
        
        //si inactif, s'assurer que la dernière ligne n'était pas un changement de sujet non plus
        if (dialogueLine != null)
          //  Debug.Log("last line warmth: " + LastLine.warmth);
        //Debug.Log("current line warmth: " + dialogueLine.warmth);
        LastLine = dialogueLine;

        //Are we reactivating a topic that was just left 
        int warmth = GetTopicActive(dialogueLine.topic);
      //  Debug.Log("line topic name: " + dialogueLine.topic + " warmth: " + warmth);
        if ((warmth == 0) && (dialogueLine.character == "Maître"))
        {
            int randomline = UnityEngine.Random.Range(0, RepriseImmediate.Length);
            string SentencePrefix = RepriseImmediate[randomline];
            Debug.Log("sentence prefix: " + SentencePrefix);
            dialogueLine.text = SentencePrefix + " " + dialogueLine.text;
        }
        foreach (Topic topic in Topics)
        {
            topic.active--;
        }

        //execute 
        //Debug.Log("outputs :" + dialogueLine.output);
        string[] outputs = dialogueLine.output.Split(new Char[] { ';' });
        ActiveTopic = "";
        foreach(string output in outputs)
        {


            //is this an attribution?
             if (output.Contains("="))
             {
                // Debug.Log("outputs " + output);
                string currentvar = "";
                 string operation = "";
                 string  value = "";
                 // let's find which variables this expression is about
                 foreach (Topic topic in Topics)
                 {

                     if (output.Contains(topic.name))
                     {
                         currentvar = topic.name;
                         //Debug.Log("var is " + currentvar);
                     }
                 }
                 //let's find out the value
                 if (output != null)
                 {
                     string[] values = output.Split(new Char[] { '=' });
                     if (values.Length > 1)
                     {
                         value = values[1];
                        //  Debug.Log("value is " + value);
                        SetTopicValue(currentvar, int.Parse(value));
                     }
                 }
             }            
            else if (output.Contains("++"))
            {
                //this is an increment
                //Find out which variable
               // Debug.Log("doing " + output);
                foreach(Topic topic in Topics)
                {
                   // Debug.Log("is it " + topic.name);
                    if (output.Contains(topic.name))
                    {
                        topic.value++;
                        topic.active = 1;
                        ActiveTopic += topic.name + ";";
                      //  Debug.Log("increase topic " + topic.name);
                    }
                }

            }
             else if (output.Contains("init"))
            {
                //we want to ++ a topic if it's currently zero
                foreach (Topic topic in Topics)
                {
                    if (output.Contains(topic.name))
                    {
                        if (topic.value == 0)
                        {
                            topic.value++;
                        }
                        //topic.active = 1;
                        //ActiveTopic += topic.name + ";";
                    }
                }

            }
            //if no operator, we just want to activate a topic
            else if(output!="")
            {
                
                bool foundtopic = false;
                foreach (Topic topic in Topics)
                {
                    if (output.Contains(topic.name))
                    {
                       // Debug.Log("Activating topic "+topic.name);
                        topic.active = 1;
                        ActiveTopic += topic.name + ";";
                        foundtopic = true;
                    }
                }
                //perhaps this is an ad hoc topic, then create it.
                if (!foundtopic)
                {
                    Topics.Add(new Topic(output, 1, 2));
                    Topics.Last<Topic>().active = 2;
                    ActiveTopic += Topics.Last<Topic>().name + ";";
                   // Debug.Log("added topic: " + Topics.Last<Topic>().name);
                }
            }


        }

        //set the current topic as active
        SetTopicActive(dialogueLine.topic);

        //default behavior is to destroy a line that has been said
        AllLines.Remove(dialogueLine);
        if (GetTopicActive("KeepTalking")<1)
        {
            if (CurrentSpeaker == "Jacques")
            {
                CurrentSpeaker = "Maître";
            }
            else
            {
                CurrentSpeaker = "Jacques";
            }
        }




    }

    public int GetTopicValue(string name)
    {
        foreach(Topic topic in Topics)
        {
            if( topic.name == name)
            {
                return topic.value;
            }
        }
        return 0;
    }
    public int GetTopicActive(string name)
    {
        foreach (Topic topic in Topics)
        {
            if (topic.name == name)
            {
                return topic.active;
            }
        }
            return 0;
    }
    public void SetTopicActive(string name)
    {
        foreach (Topic topic in Topics)
        {
            if (topic.name == name)
            {
                topic.active = 1;
            }
        }
    }
    public void SetTopicValue(string name, int value)
    {
        foreach (Topic topic in Topics)
        {
            if (topic.name == name)
            {
                topic.value = value;
            }
        }
    }

    public void SaveState()
    {
        /*if (CurrentSpeaker == "Jacques")
        {
            PlayerPrefs.SetString("LastJacquesLine", LastLine.text);
        }*/
        //PlayerPrefs.SetString("CurrentSpeaker", CurrentSpeaker);
        Debug.Log("Playerprefs : SAVING");
        foreach(Topic topic in Topics)
        {
            PlayerPrefs.SetFloat(topic.name, topic.value);
            PlayerPrefs.SetFloat(topic.name + "active", topic.active);
            //Debug.Log(topic.name + " : " + topic.value);
        }


    }
    public void RestoreState()
    {
        CurrentSpeaker = "Jacques";
        Debug.Log("Playerprefs : RESTORING");
        foreach (Topic topic in Topics)
        {
            bool haskey = PlayerPrefs.HasKey(topic.name);
            if (haskey)
            {
                topic.value = Mathf.RoundToInt(PlayerPrefs.GetFloat(topic.name,0));
                topic.active = Mathf.RoundToInt(PlayerPrefs.GetFloat(topic.name + "active",0));
            }
            else
            {
                topic.value = 0;
                topic.active = 0;
                PlayerPrefs.SetFloat(topic.name,0);
                PlayerPrefs.SetFloat(topic.name + "active",0);

            }

            //Debug.Log(topic.name + " : " + topic.value);
        }
    }
}

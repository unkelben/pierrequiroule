﻿#pragma strict

public var speed:float = 1;
//public var target:Transform;
private var talking = false;
public var ballhit:AudioClip;
public var snowwhite:AudioClip;
var rb:Rigidbody;
var touchedplayer:boolean = false;
var grounded:boolean = false;
var guy:MoveAround;
private var fadingout:boolean = false;

function Start () {
	rb = GetComponent.<Rigidbody>();
	Time.timeScale = 0.7;
}

function Update () {
	if(talking&&grounded&&!fadingout){
		GetComponent.<Rigidbody>().velocity=Vector3.zero;
		var targetDir = Camera.main.transform.position - transform.position;
		//transform.LookAt(Camera.main.transform.position);
		var newDir = Vector3.RotateTowards(transform.forward,targetDir,speed, 0.0);
		transform.rotation = Quaternion.LookRotation(newDir);
	}
	if(touchedplayer){
		if(rb.velocity.magnitude>30){
			Debug.Log("clamping");
			rb.velocity*=0.9;
		}
	}
	if(guy!=null){
		if(guy.FadingOutScene&&!fadingout){
			fadingout = true;
			fadeOutSound(guy.fadeOutTime);
		}
	}
	//Debug.Log("velocity: "+rb.velocity.magnitude);
}
function fadeOutSound(fadeOutTime:float){
//	yield WaitForSeconds(2.5);

	Debug.Log("Fading out ball sound.");

	var yieldTime = Time.deltaTime;

	while(GetComponent.<AudioSource>().volume > 0) {
		GetComponent.<AudioSource>().volume -= yieldTime / fadeOutTime;
//		Debug.Log(GetComponent.<AudioSource>().volume);
		yield WaitForSeconds(yieldTime);
	}

}
function OnCollisionExit(col:Collision){
	if(col.collider.gameObject.tag == "ground")
		grounded = false;

}

function OnCollisionEnter (col:Collision) {
	Debug.Log(col.collider.gameObject.tag);
	if(col.collider.gameObject.tag == "ground")
		grounded = true;

	if (col.collider.gameObject.tag == "guy" && !talking && !touchedplayer) {
		touchedplayer = true;
		Time.timeScale = 1;
		guy = col.gameObject.GetComponent.<MoveAround>();

		GetComponent.<AudioSource>().clip = ballhit;
		GetComponent.<AudioSource>().Play();
		yield WaitForSeconds(2);
		talking = true;
		//GetComponent.<Rigidbody>().isKinematic = true;
		GetComponent.<AudioSource>().clip = snowwhite;
		GetComponent.<AudioSource>().Play();
	}
}

function OnTriggerEnter (col:Collider) {
	if (col.gameObject.tag == "catchball") {
		Debug.Log("Ball fell off the world...");
		fadeOutSound(3);
	}
}


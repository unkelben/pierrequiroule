﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RepliqueBehavior : MonoBehaviour {


    public Text Texte;
    public bool moving = false;
    public bool timetomove = false;
    public float movestep = 0;
    public bool initialposition = false;
    public float MoveDuration = 1f;
    public Color FadeColor;
	
    // Use this for initialization
	void Start () {
        //Name = GetComponent<Text>();
        //Debug.Log("new replique size " + GetComponent<RectTransform>().rect.height);
 
        StartCoroutine("FadeIn");
        // MoveUp();
    }
    public void StartFade()
    {
        StartCoroutine("FadeIn");
    }
    void Update()
    {
        /*if((!initialposition)&&(movestep!=0))
        {
            GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -2000 - movestep);
            initialposition = true;
        }*/
        if (timetomove&&initialposition)
        {
            timetomove = false;
            moving = true;
            MoveUp();
        }
    }

    IEnumerator FadeIn()
    {
        //yield return new WaitForSeconds(1f);
        if (Texte != null)
        {
            while (Texte.color.a < 0.99f)
            {
                // Debug.Log("alpha: " + Name.color);
                Texte.color = Texte.color + new Color(0, 0, 0, 0.05f);
                yield return new WaitForSeconds(0.01f);
            }
        }
        else
        {
            while (GetComponent<Image>().color.a < 0.99f)
            {
                // Debug.Log("alpha: " + Name.color);
                GetComponent<Image>().color = GetComponent<Image>().color + new Color(0, 0, 0, 0.05f);
                yield return new WaitForSeconds(0.01f);
            }
        }



    }
    public void MoveUp()
    {
        
        float YDestination = GetComponent<RectTransform>().anchoredPosition.y-50 + movestep;
        
       // Debug.Log("movestep:" + movestep);            
        StartCoroutine(GotoY(YDestination));

        
        
    }
    IEnumerator GotoY(float desty)
    {
        RectTransform myrect = GetComponent<RectTransform>();
        moving = true;
       // Debug.Log("my pos:" + myrect.anchoredPosition.y);
       // Debug.Log("desty : " + desty);
        Vector3 MoveStep = new Vector3(0,(desty-myrect.anchoredPosition.y)*(0.01f/MoveDuration),0);
       // Debug.Log("movestep : " + MoveStep);
        while (myrect.anchoredPosition.y < desty)
        {
           
            myrect.Translate(MoveStep);
           /* if (myrect.anchoredPosition.y > 200)
            {
                GameObject.Find("GUI").GetComponent<GUIManager>().Repliques.Remove(transform);
                GameObject.Find("GUI").GetComponent<GUIManager>().RepliquesMoving--;
                Destroy(gameObject);
            }*/
            yield return new WaitForSeconds(0.01f);
           // Debug.Log("my pos:" + myrect.anchoredPosition.y);
        }
        //Debug.Log("finished moving");
        
        yield return new WaitForSeconds(0.5f);
        GameObject.Find("GUI").GetComponent<GUIManager>().RepliquesMoving--;
        moving = false;
        

    }

}

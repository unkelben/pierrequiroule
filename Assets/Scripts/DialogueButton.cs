﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueButton : MonoBehaviour {

    public DialogueLine line;


    ConversationManager CM;
    GUIManager GM;
    public Text ButtonText;


    // Use this for initialization
    void Start () {
        CM = GameObject.Find("ConversationManager").GetComponent<ConversationManager>();
        GM = GameObject.Find("GUI").GetComponent<GUIManager>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetButton(DialogueLine _line)
    {
        line = _line;
        ButtonText.text = line.shorttext;
    }

    public void SayLine()
    {
        GM.SayLine(line);
        
    }

    public void NextLine()
    {
        GM.NPCTalk();
    }
}

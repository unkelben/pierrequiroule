﻿#pragma strict
public var minimumDistance:float = 1.25f;
public var frontBackSpeed:float;
public var leftRightSpeed:float;
public var boredomSpeedModifier:float = 0.25f;
private var currentFrontBackSpeed:float;
private var currentLeftRightSpeed:float;

private var speed:float;
public var moving:boolean=false;
public var interrupt:boolean = false;
public var userInterrupt:boolean = false;
private var rb:Rigidbody;
public var orientationScript:ChangeSpriteOrientation;
public var hasFallen = false;
//public var thrower:ballthrower;
//public var lightning:GameObject;
public var mainlight:GameObject;
public var followspot:GameObject;
private var lightcolor:Color;

public var initialimpulse:float;
public var ReadyToChangeScene:boolean = false;
private var ChangingScene:boolean = false;
public var FadingOutScene:boolean = false;
public var NextScene:String;
public var autopilot:boolean = false;

public var screenfadeout:UnityEngine.UI.Graphic;
public var preFadeOutDelay:float = 2;
public var fadeOutTime:float = 2;

public var grounded:boolean = false;
public var groundcheck:Transform;

public var human = true;



function Start () {
	currentFrontBackSpeed = frontBackSpeed;
	currentLeftRightSpeed = leftRightSpeed;
	rb = GetComponent.<Rigidbody>();
    //rb.isKinematic = true;
    //if in immersion, push him a little for kinetic consistency with wind gust in last scene
	
	
}





function OnCollisionEnter(collision:Collision){

    /*if(!ProgramStarted){
        GetComponent.<AudioSource>().Play();
        yield WaitForSeconds(3);
        PierreQuiRoule();
    }*/

}


function Update () {
   /* if((Vector3.Distance(transform.position,Roche.position)>20)&&sequenceroche){
        sequenceroche = false;
        EndOfDemo = false;
        PierreQuiRoule();
    }*/
   /* if(transform.position.y<-20){
        interrupt = true;
        EndOfDemo = true;
        finale = true;
        
    }
    if(finale){
        transform.LookAt(transform.position+new Vector3(0,0,-100));
    }
    
    if((Vector3.Distance(transform.position,Roche.position)<30)&&!sequenceroche){
        Debug.Log("roche arrive");
        EndOfDemo = true;
        
        sequenceroche = true;
        RegardeRoche();
    }
    */

    grounded = Physics.Linecast(transform.position, groundcheck.position, 1 << LayerMask.NameToLayer("ground"));
    if(!grounded){
    	//Debug.Log("not grounded");
        moving = false;
        if(!autopilot )
            StopCoroutine("moveTo");
    }
		
	if (Input.GetMouseButtonDown(0)&&moving&&!autopilot){
	    moving = false;
	    StopCoroutine("moveTo");
		//interruptWalking();
	}
    if (Input.GetMouseButtonDown(0)&&hasFallen&&!autopilot)
    {
	
        var hit:RaycastHit;
        var ray:Ray= Camera.main.ScreenPointToRay(Input.mousePosition);
        var layermask = 1 << LayerMask.NameToLayer("ground");
        if (Physics.Raycast(ray,  hit, Mathf.Infinity, layermask))
        {
            
//            Debug.Log(hit.point);
           // transform.LookAt(Vector3(hit.point.x,transform.position.y,hit.point.z));

           var feet = new Vector3(transform.position.x,transform.position.y - GetComponent.<MeshFilter>().mesh.bounds.extents.y,transform.position.z);
           if (Vector3.Distance(feet,hit.point) > minimumDistance) {
            	StartCoroutine("moveTo",hit.point);
           }
        }
        
    

  
    }

}

function interruptWalking(){
	interrupt = true;
	yield WaitForSeconds(0.01);
}



function faceCamera(){

    transform.LookAt(Camera.main.transform);

}
public function stopMoveTo(){
	StopCoroutine("moveTo");
}
function startMoveTo(destination:Vector3){
	StartCoroutine("moveTo",destination);
}

function moveTo(destination:Vector3){
  //  Debug.Log("moving");
  	
    moving = true;



	//Initial surge of push to move against wind in cybertext scene
        transform.LookAt(Vector3(destination.x,transform.position.y,destination.z));
        rb.AddRelativeForce(Vector3.forward*initialimpulse,ForceMode.Impulse);
        //transform.Translate(Vector3.forward * Time.deltaTime * speed*4);

    while((Vector3.Distance(transform.position,Vector3(destination.x,transform.position.y,destination.z))>0.2)&&!interrupt){
        //Debug.Log("distance:"+Vector3.Distance(transform.position,destination));
        
        if(!human&&(Vector3.Distance(transform.position,Vector3(destination.x,transform.position.y,destination.z))<4)){
            break;
        }

        transform.LookAt(Vector3(destination.x,transform.position.y,destination.z));
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
        //rb.AddForce(Vector3.forward*speed*Time.deltaTime,ForceMode.VelocityChange);
        		

        if (orientationScript.currentAnim == orientationScript.walkRight || orientationScript.currentAnim == orientationScript.walkLeft) 
        {
    		speed = currentLeftRightSpeed;
	    }
	    else {
	    	speed = currentFrontBackSpeed;
	    }


        yield (WaitForSeconds(0.01));
    }

    if (!autopilot && !ReadyToChangeScene) 
    {
    	rb.velocity=Vector3.zero;
   	}
    interrupt = false;
    moving = false;
    autopilot = false;

}
# _Jacques le fataliste_  (dossier de presse)
## Cette version pour téléphone intelligent du _Jacques le fataliste_ de Diderot permet au lecteur de jouer le rôle du maître dans une conversation interactive avec son valet Jacques.

### En quelques mots
- Une adaptation interactive de [_Jacques le Fataliste et son maître_](https://fr.wikipedia.org/wiki/Jacques_le_Fataliste_et_son_ma%C3%AEtre), un roman classique du XVIIIe siècle écrit par Denis Diderot.
- Disponible gratuitement pour les téléphones Android et iOS à partir du 16 juillet 2019.
- Développé par le [LabLabLab](https://lablablab.net), un groupe de recherche sur les conversations interactives à l'université Concordia.
- Une interface inspirée des applications de "chat" à la Messenger.
- Une conversation à la fois drôle et profonde sur l'amour et le destin.

![logo](https://lablablab.net/images/splash.png "Jacques--Intro")     ![logo](https://lablablab.net/images/texte2.png "Jacques--Texte") ![logo](https://lablablab.net/images/illustration.png "Jacques--Illustration")

### Une oeuvre "non-linéaire" au XVIIIe siècle
_Jacques le fataliste_ est une conversation entre Jacques, un valet bavard, et son maître. L'oeuvre classique de Diderot a déjà été reconnnue comme étant très moderne dans sa forme: les personnages ne sont pas présentés, on ne sait pas où ils vont... tout se concentre sur leur conversation, les histoires qu'ils se racontent--en particulier celle des amours de Jacques dont la narration est sans cesse interrompue par des digressions.

Bien que rédigée plus de 200 ans avant l'avènement du jeu vidéo et de la fiction interactive, _Jacques le fataliste_ présente déjà plusieurs caractéristiques propres aux conversations interactives, et particulièrement la **non-linéarité**. Tel un joueur de jeu vidéo, le personnage du maître "explore" les différentes histoires de son valet à son gré, l'interrompant, sautant d'un sujet à l'autre, sans se soucier de ses intentions "d'auteur". Il y a là quelque chose de très similaire à l'écriture de dialogues interactifs dont la difficulté réside en ce que les auteurs ne savent jamais les avenues qui intéresseront les joueurs et dans quel ordre elles seront explorées.

### Un "paysage conversationnel" interactif
Au LabLabLab, nous appelons "paysage conversationnel" une conversation interactive permettant d'explorer différents sujets au gré de sa curiosité, sans se soucier de devoir aboutir quelque part en particulier, sans égards aux canons de la narration classique.

Voyant dans _Jacques le fataliste_ un précurseur de ce genre de situation, nous avons trouvé intéressant d'en faire une adaptation interactive. Dans cette application pour téléphone, le joueur est placé dans la situation du maître et peut à son tour interrompre Jacques, lui demander de poursuivre tel récit plutôt qu'un autre, bref: explorer ce paysage conversationel à son gré. Peut-être est-ce ainsi que Diderot aurait envisagé son oeuvre s'il l'avait écrite aujourd'hui?

### Une interface inspirée de Messenger
À l'ère du téléphone intelligent, les applications de clavardage ("chat") comme Messenger représentent un des paradigmes majeurs de la conversation textuelle. En adaptant _Jacques le Fataliste_ pour cette plateforme, il semblait naturel d'en reprendre les principaux principes. On peut imaginer que cette forme puisse rendre l'oeuvre plus accessible à certains jeunes lecteurs.

### Le LabLabLab - groupe de recherche en conversations interactives
Le [LabLabLab](https://www.lablablab.net) est un groupe de recherche intéressé par les conversations interactives avec des personnages virtuels dans le contexte du jeu vidéo et de la fiction interactive. Il a été fondé par [Jonathan Lessard](https://jonathanlessard.net) en 2013. La principale méthodologie du groupe est la recherche-création: les hypothèses de recherche sont testées sous forme de jeux entièrement réalisés et mis à la disponibilité du public. Le LabLabLab s'est intéressé à l'interaction en langage naturel (chatbots) et a créé trois jeux dont un [simulateur de prophète](https://www.lablablab.net/?p=437) et une adaptation de [Blanche Neige](https://www.lablablab.net/?p=435). Son dernier projet majeur était [Hammurabi](https://www.lablablab.net/?p=605) qui utilise une technologie de génération de texte pour permettre à des personnages virtuels de s'exprimer sur des contextes imprévisibles au moment de l'écriture.

### Jonathan Lessard - Professeur et concepteur de jeux vidéo expérimentaux
[Jonathan Lessard](https://jonathanlessard.net/) est professeur-chercheur-créateur en design de jeu et mondes virtuels au département de design et art numérique de l'Université Concordia. Il dirige le groupe de recherche [LabLabLab](https://www.lablablab.net) et s'intéresse également à l'histoire des jeux. Il s'est récemment commis dans la fiction traditionnelle avec un roman publié chez Les Malins: le [Douchebag de l'espace](https://www.lesmalins.ca/fr/livre/jeunesse/1437-le-douchebag-de-l-espace.html).  

### Contacter Jonathan Lessard : jonathan.lessard@concordia.ca
